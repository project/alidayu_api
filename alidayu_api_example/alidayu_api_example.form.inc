<?php

/**
 * @file
 * Form of Alidayu API example.
 */

/**
 * Alidayu API example form.
 */
function alidayu_api_example_form() {

  $form = array();

  $form['sms_template_code'] = array(
    '#type' => 'textfield',
    '#title' => 'SmsTemplateCode',
    '#description' => t('SMS Template code, e.g. SMS_xxxxxxxx'),
    '#size' => 13,
    '#required' => TRUE,
  );

  $form['rec_num'] = array(
    '#type' => 'textfield',
    '#title' => 'RecNum',
    '#description' => t('Phone number to receive this SMS message, e.g. 13xxxxxxxxx'),
    '#size' => 12,
    '#maxlength' => 11,
    '#required' => TRUE,
  );

  $form['free_sign_name'] = array(
    '#type' => 'textfield',
    '#title' => 'FreeSignName',
    '#description' => t('Verified sign name in Alidayu platform'),
    '#size' => 8,
    '#required' => TRUE,
  );

  $form['param_string'] = array(
    '#type' => 'textfield',
    '#title' => 'ParamString',
    '#description' => t("Param string in JSON format, e.g. {code:'123456'}"),
    '#size' => 20,
    '#required' => TRUE,
  );

  $form['send'] = array(
    '#type' => 'submit',
    '#value' => 'Send',
  );

  return $form;
}

/**
 * Alidayu API example form submit.
 */
function alidayu_api_example_form_submit($form, &$form_status) {
  $values = $form_status['values'];
  alidayu_api_send($values['sms_template_code'], $values['rec_num'], $values['param_string'], $values['free_sign_name']);
}
