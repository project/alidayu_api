Alidayu (阿里大于) is an SMS services platform founded by Alibaba Group
(NYSE:BABA), to provide stable, reliable and fast SMS services for Chinese
mobile users. It can be used to send SMS messages, verification codes, bulk
messages and audio messages to mobile users in China. China Telecom(中国电信)、
China Unicom(中国联通) and China Mobile(中国移动) and other virutal SPs in China
 are supported. For more information about Alidayu, please visit website:
 https://www.alidayu.com

Alidayu API (alidayu_api) module integrate Alidayu services with Drupal,
to send SMS message to users via Alidayu gateway.

API
Developer can use alidayu_api_send($sms_template_code, $phone_num, $param, $sign
); to send an SMS message.

This project is sponsored by: [TheRiseIT, Inc](https://www.theriseit.com)
