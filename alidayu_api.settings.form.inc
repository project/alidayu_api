<?php

/**
 * @file
 * Settings form of Alidayu API.
 */

/**
 * Alidayu API settings form.
 */
function alidayu_api_settings_form() {

  $form = array();

  $form['alidayu_api_appkey'] = array(
    '#type' => 'textfield',
    '#title' => 'App Key',
    '#default_value' => variable_get('alidayu_api_appkey', ''),
    '#require' => TRUE,
  );

  $form['alidayu_api_secret'] = array(
    '#type' => 'textfield',
    '#title' => 'App Secret',
    '#default_value' => variable_get('alidayu_api_secret', ''),
    '#require' => TRUE,
  );

  return system_settings_form($form);
}
